﻿using System.Web.Http;

namespace HbApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();           
        }
    }
}
