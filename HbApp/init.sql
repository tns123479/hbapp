USE [master]
GO

/****** Object:  Database [hb]    Script Date: 04/04/2017 10:40:52 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'hb')
DROP DATABASE [hb]
GO

USE [master]
GO

/****** Object:  Database [hb]    Script Date: 04/04/2017 10:40:52 ******/
CREATE DATABASE [hb] ON  PRIMARY 
( NAME = N'hb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\hb.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'hb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\hb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [hb] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [hb] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [hb] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [hb] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [hb] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [hb] SET ARITHABORT OFF 
GO

ALTER DATABASE [hb] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [hb] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [hb] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [hb] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [hb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [hb] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [hb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [hb] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [hb] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [hb] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [hb] SET  DISABLE_BROKER 
GO

ALTER DATABASE [hb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [hb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [hb] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [hb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [hb] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [hb] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [hb] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [hb] SET  READ_WRITE 
GO

ALTER DATABASE [hb] SET RECOVERY FULL 
GO

ALTER DATABASE [hb] SET  MULTI_USER 
GO

ALTER DATABASE [hb] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [hb] SET DB_CHAINING OFF 
GO

USE [hb]
GO

/****** Object:  Table [dbo].[Country]    Script Date: 04/04/2017 10:44:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Country](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](100) NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[City]    Script Date: 04/04/2017 10:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[City]') AND type in (N'U'))
DROP TABLE [dbo].[City]
GO

/****** Object:  Table [dbo].[City]    Script Date: 04/04/2017 10:45:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[City](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [nvarchar](100) NOT NULL,
	[CountryId] [int] NULL,
 CONSTRAINT [PK_city] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO





IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_employeer_city]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] DROP CONSTRAINT [FK_employeer_city]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_employeer_country]') AND parent_object_id = OBJECT_ID(N'[dbo].[Employee]'))
ALTER TABLE [dbo].[Employee] DROP CONSTRAINT [FK_employeer_country]
GO

/****** Object:  Table [dbo].[Employee]    Script Date: 04/04/2017 10:46:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
DROP TABLE [dbo].[Employee]
GO

/****** Object:  Table [dbo].[Employee]    Script Date: 04/04/2017 10:46:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
	[SurName] [nvarchar](100) NULL,
	[Phone] [nvarchar](20) NULL,
	[Email] [nvarchar](50) NOT NULL,
	[CountryId] [int] NULL,
	[CityId] [int] NULL,
 CONSTRAINT [PK_employeer] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_city_country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO

ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_city_country]
GO


ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_employeer_city] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_employeer_city]
GO

ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_employeer_country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([CountryId])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_employeer_country]
GO


INSERT INTO [dbo].[Country] (CountryName) VALUES ('���������')
GO
INSERT INTO [dbo].[Country] (CountryName) VALUES ('������')
GO

INSERT INTO [dbo].City(CityName, CountryId) VALUES ('������',1)
GO
INSERT INTO [dbo].City(CityName, CountryId) VALUES ('������',1)
GO
INSERT INTO [dbo].City(CityName, CountryId) VALUES ('������',2)
GO
INSERT INTO [dbo].City(CityName, CountryId) VALUES ('�����-���������',2)
GO