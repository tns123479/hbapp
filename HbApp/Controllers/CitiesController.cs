﻿using System.Collections.Generic;
using System.Web.Http;
using HbApp.Models;
using HbApp.Repositories;

namespace HbApp.Controllers
{
    [RoutePrefix("api/Cities")]
    public class CitiesController : ApiController
    {
        CityRepository repository = new CityRepository();
        [Route("")]
        public IEnumerable<City> Get()
        {
            return repository.Get();
        }

        [Route("{id:int}")]
        public City Get(int id)
        {
            return repository.GetById(id);
        }

        [Route("ByCountry/{CountryId:int}")]
        public IEnumerable<City> GetCitiesByCountry(int countryId)
        {
            return repository.GetByCountry(countryId);
        }
        
    }
}
