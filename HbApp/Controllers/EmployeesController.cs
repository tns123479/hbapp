﻿using System.Collections.Generic;
using System.Web.Http;
using HbApp.Models;
using HbApp.Repositories;

namespace HbApp.Controllers
{
    [RoutePrefix("api/Employees")]
    public class EmloyeesController : ApiController
    {
        EmployeeRepository repository = new EmployeeRepository();
        [Route("")]
        public IEnumerable<Employee> Get()
        {
            return repository.Get();
        }

        [Route("{id:int}")]
        public Employee Get(int id)
        {
            return repository.GetById(id);
        }

        [Route("add")]
        [HttpPost]
        public void Add([FromBody]Employee employee)
        {
            repository.AddEmployee(employee);
        }

        [Route("search")]
        [HttpPost]
        public IEnumerable<Employee> Search([FromBody]Employee employee)
        {
            return repository.Search(employee);
        }

        [Route("save")]
        [HttpPost]
        public void Save([FromBody]Employee employee)
        {
            repository.SaveEmployee(employee);
        }

        [Route("del/{employeeId:int}")]
        [HttpPost]
        public void Delete(int employeeId)
        {
            repository.DeleteEmployee(employeeId);
        }
    }
}
