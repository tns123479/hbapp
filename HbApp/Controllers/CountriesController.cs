﻿using System.Collections.Generic;
using System.Web.Http;
using HbApp.Models;
using HbApp.Repositories;

namespace HbApp.Controllers
{
    [RoutePrefix("api/Countries")]
    public class CountriesController : ApiController
    {
        CountryRepository repository = new CountryRepository();
        [Route("")]
        public IEnumerable<Country> Get()
        {
            return repository.Get();
        }

        [Route("{id:int}")]
        public Country Get(int id)
        {
            return repository.GetById(id);
        }
    }
}
