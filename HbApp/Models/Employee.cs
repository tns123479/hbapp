﻿namespace HbApp.Models
{
    public class Employee
    {
        public virtual int EmployeeId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string SurName { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual int? CountryId { get; set; }
        public virtual int? CityId { get; set; }

    }
}