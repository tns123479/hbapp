﻿namespace HbApp.Models
{
    public class Country
    {
        public virtual int CountryId { get; set; }
        public virtual string CountryName { get; set; }
    }
}