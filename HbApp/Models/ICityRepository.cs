﻿using System.Collections.Generic;

namespace HbApp.Models
{
    interface ICityRepository
    {
        City GetById(int id);
        ICollection<City> Get();
        ICollection<City> GetByCountry(int countryId);
    }
}
