﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HbApp.Models
{
    interface ICountryRepository
    {
        Country GetById(int countryId);
        ICollection<Country> Get();
    }
}
