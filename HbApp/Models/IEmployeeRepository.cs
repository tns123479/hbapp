﻿using System.Collections.Generic;

namespace HbApp.Models
{
    interface IEmployeeRepository
    {
        Employee GetById(int employeeId);
        ICollection<Employee> Get();
        ICollection<Employee> Search(Employee employee);
    }
}
