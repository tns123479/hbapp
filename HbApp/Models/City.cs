﻿namespace HbApp.Models
{
    public class City
    {
        public virtual int CityId { get; set; }
        public virtual string CityName { get; set; }
        public virtual int CountryId { get; set; }
    }
}