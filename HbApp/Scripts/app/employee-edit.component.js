﻿angular.
    module('employee').
    component('employeeEdit', {
        templateUrl: 'Scripts/templates/employee-edit.html',
        controller: ['$routeParams', '$http', '$window', '$scope', 'EmployeeService', 'CountryService',
            function EmployeeEditController($routeParams, $http, $window, $scope, EmployeeService, CountryService) {
                var self = this;
                var employeeId = $routeParams.employeeId;
                $scope.employee = {};
                $scope.seletedCountry = null;
                $scope.seletedCity = null;
                $scope.countries = [];
                $scope.cities = [];

                EmployeeService.getById(employeeId, function (employee) {
                    $scope.employee = employee;

                    $scope.seletedCountry = {};
                    $scope.seletedCountry.CountryId = $scope.employee.CountryId;
                    $scope.seletedCity = {};
                    $scope.seletedCity.CityId = $scope.employee.CityId;

                    loadCities($scope.employee.CountryId);
                })               

                var loadCities = function (countryId) {
                    if (!countryId) {
                        $scope.cities = [];
                        return;
                    }

                    $http.get('api/cities/bycountry/' + countryId).then(function (response) {
                        $scope.cities = [];
                        if (response.data) {
                            $scope.cities = response.data
                        }
                    })
                }

                CountryService.load(function (countries) {                    
                    $scope.countries = countries                    
                })

                $scope.changeCountry = function changeCountry() {
                    if ($scope.seletedCountry) {
                        $scope.employee.CountryId = $scope.seletedCountry.CountryId;
                    } else {
                        $scope.employee.CountryId = null;
                    }

                    $scope.employee.CityId = null;
                    $scope.seletedCity = null;
                    loadCities($scope.employee.CountryId);
                }

                $scope.back = function back() {
                    $window.location.href = '/';
                }

                $scope.save = function save() {
                    if ($scope.seletedCity) {
                        $scope.employee.CityId = $scope.seletedCity.CityId;
                    }
                    if ($scope.seletedCountry) {
                        $scope.employee.CountryId = $scope.seletedCountry.CountryId;
                    }
                    $http.post('api/employees/save', $scope.employee).then(function (response) {
                        $window.location.href = '/';
                    })
                }
            }
        ]
    });