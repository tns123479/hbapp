﻿angular.
    module('employee').
    component('employeeList', {
        templateUrl: 'Scripts/templates/employee-list.html',
        controller: ['$routeParams', '$http', '$scope', 'EmployeeService',
            function EmployeeListController($routeParams, $http, $scope, EmployeeService) {
                $scope.isSearch = false;
                $scope.employees = [];
                $scope.searchData = {};

                var onEmployeesLoad = function (employees) {
                    $scope.employees = employees;
                }

                var loadData = function (params) {
                    if (params) {
                        EmployeeService.search(params, onEmployeesLoad);
                    } else {
                        EmployeeService.load(onEmployeesLoad);
                    }
                }               
                
                $scope.deleteEmployee = function deleteEmployee(employeeId) {
                    if (confirm('Удалить запись?')) {                        
                        EmployeeService.delete(employeeId, function () {
                            var len = $scope.employees.length;
                            var ind = -1;
                            for (var i = 0; i < len; i++) {
                                var e = $scope.employees[i];
                                if (e.EmployeeId == employeeId) {
                                    ind = i;
                                    break;
                                }
                            }
                            if (ind != -1) {
                                $scope.employees.splice(ind, 1);
                            }
                        })
                    }                    
                };

                $scope.search = function search() {
                    loadData($scope.searchData);
                };

                $scope.clearSearch = function search() {
                    $scope.searchData = null
                    loadData(null);                    
                };

                loadData(null);
            }
        ]
    });