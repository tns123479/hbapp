﻿angular.
    module('employee').
    component('employeeAdd', {
        templateUrl: 'Scripts/templates/employee-add.html',
        controller: ['$routeParams', '$http', '$window', '$scope', 'CountryService', 'EmployeeService',
            function EmployeeAddController($routeParams, $http, $window, $scope, CountryService, EmployeeService) {
                var self = this;
                $scope.employee = { CountryId: null };
                $scope.seletedCountry = null;
                $scope.countries = [];
                $scope.cities = [];
                $scope.employeeValidation = {
                    valid: true,
                    messages: [],
                    add: function (err) {
                        this.messages.push(err); this.valid = false;
                    },
                    clear: function () {
                        this.messages = [];
                        this.valid = false;
                    }
                };

                var loadCities = function (countryId) {
                    if (!countryId) {
                        $scope.cities = [];
                        return;
                    }
                        
                    $http.get('api/cities/bycountry/' + countryId).then(function (response) {                        
                        $scope.cities = response.data;
                    })
                }

                CountryService.load(function (data) {
                    $scope.countries = data;
                })   

                $scope.back = function back() {
                    $window.location.href = '/';
                }

                $scope.changeCountry = function changeCountry() {
                    if ($scope.seletedCountry) {
                        $scope.employee.CountryId = $scope.seletedCountry.CountryId;
                    } else {
                        $scope.employee.CountryId = null;
                    }
                    
                    $scope.employee.CityId = null;
                    loadCities($scope.employee.CountryId);
                }

                var formValidation = function () {
                    $scope.employeeValidation = EmployeeService.validate($scope.employee);
                    return $scope.employeeValidation.valid;
                }

                $scope.add = function save() {
                    if (formValidation()) {
                        EmployeeService.add($scope.employee, function (response) {
                            $scope.back();
                        })
                    }                    
                }
                
            }
        ]
    });