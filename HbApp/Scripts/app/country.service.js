﻿angular.
    module('employee').
    factory('CountryService', ['$http', function ($http) {
        var api = {};

        api.load = function (onOk) {
            $http.get('api/countries').then(function (response) {                    
                onOk(response.data);
            })
        }

        return api;
    }])