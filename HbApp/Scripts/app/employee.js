﻿angular.
    module('employee', ['ngRoute']).
    config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.
                when('/', {
                    template: '<employee-list></employee-list>'
                }).
                when('/employee/edit/:employeeId', {
                    template: '<employee-edit></employee-edit>'
                }).
                when('/employee/add', {
                    template: '<employee-add></employee-add>'
                }).
                otherwise({
                    redirectTo: '/'
                });
        }
    ]);
