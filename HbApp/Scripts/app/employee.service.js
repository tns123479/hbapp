﻿angular.
    module('employee').
    factory('EmployeeService', ['$http', function ($http) {
        var api = {};

        api.load = function (onOk) {
            $http.get('api/employees').then(function (response) {    
                if (onOk) {
                    onOk(response.data);
                }                
            })
        }

        api.search = function (params, onOk) {
            $http.post('api/employees/search', params).then(function (response) {
                if (onOk) {
                    onOk(response.data);
                }
            })
        }

        api.delete = function (employeeId, onOk) {
            $http.post('api/employees/del/' + employeeId).then(function (response) {
                if (onOk) {
                    onOk(response.data);
                }
            })
        }

        api.add = function (employee, onOk) {            
            $http.post('api/employees/add', employee).then(function (response) {
                if (onOk) {
                    onOk(response.data);
                }
            })
        }

        api.getById = function (employeeId, onOk) {
            $http.get('api/employees/' + employeeId).then(function (response) {
                if (onOk) {
                    onOk(response.data);
                }
            })
        }

        api.validate = function (employee) {
            var messages = [];

            var isEmpty = function (val) {
                if (val == '') return true;
                if (val == 'undefined') return true;
                if (val == null) return true;
                return false;
            }
                    
            if (isEmpty(employee.FirstName)) {
                messages.push('Поле Фамилия обязательно для заполнения');
            }
            if (!isEmpty(employee.Phone)) {
                if (!employee.Phone.match(/^\d+/)) {
                    messages.push('Поле Телефон должно содержать только цифры');
                }
            }
            

            if (isEmpty(employee.Email)) {
                messages.push('Поле Email обязательно для заполнения');
            } else {
                var atpos = employee.Email.indexOf("@");
                var dotpos = employee.Email.lastIndexOf(".");
                console.info('atpos');
                console.info(atpos);
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= employee.Email.length) {
                    messages.push('Неверный адрес электронной почты');
                }
            }
            
            return {
                valid: (messages.length == 0),
                messages: messages
            };
        }

        return api;
    }])