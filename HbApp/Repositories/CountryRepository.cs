﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HbApp.Models;

namespace HbApp.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        public ICollection<Country> Get()
        {
            IList<Country> list = new List<Country>();
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<Country>();
                    list = criteria.List<Country>();
                }
            }
            return list;
        }

        public Country GetById(int countryId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
                return session.Get<Country>(countryId);
        }
    }
}