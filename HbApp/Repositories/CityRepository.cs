﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HbApp.Models;

namespace HbApp.Repositories
{
    public class CityRepository : ICityRepository
    {
        public ICollection<City> Get()
        {
            IList<City> list = new List<City>();
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<City>();
                    list = criteria.List<City>();
                }
            }
            return list;
        }

        public City GetById(int cityId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
                return session.Get<City>(cityId);
        }

        public ICollection<City> GetByCountry(int countryId)
        {
            IList<City> list = new List<City>();
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    list = session.QueryOver<City>()
                        .Where(x => x.CountryId == countryId)
                        .List();
                }
            }
            return list;
        }
    }
}