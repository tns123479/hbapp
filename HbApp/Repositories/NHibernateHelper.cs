﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HbApp.Models;

namespace HbApp.Repositories
{
    public class NHibernateHelper
    {
        public static ISession  OpenSession()
        {
            ISessionFactory sessionFactory = Fluently.Configure()
             .Database(MsSqlConfiguration.MsSql2008.
             ConnectionString(c => c.FromConnectionStringWithKey("hb-connection"))
                    .ShowSql()
                    )
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Country>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<City>())
                    .BuildSessionFactory();
                    return sessionFactory.OpenSession();
                }
    }
}