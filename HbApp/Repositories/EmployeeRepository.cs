﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using HbApp.Models;

namespace HbApp.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public ICollection<Employee> Get()
        {
            IList<Employee> list = new List<Employee>();
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<Employee>();
                    list = criteria.List<Employee>();
                }
            }
            return list;
        }

        public Employee GetById(int employeeId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
                return session.Get<Employee>(employeeId);
        }

        public ICollection<Employee> Search(Employee employee)
        {
            IList<Employee> list = new List<Employee>();
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<Employee>();
                    if (employee.LastName != "")
                    {
                        criteria.Add(Restrictions.InsensitiveLike("LastName", employee.LastName, MatchMode.Anywhere));
                    }
                    if (employee.FirstName != "")
                    {
                        criteria.Add(Restrictions.InsensitiveLike("FirstName", employee.FirstName, MatchMode.Anywhere));
                    }
                    if (employee.SurName != "")
                    {
                        criteria.Add(Restrictions.InsensitiveLike("SurName", employee.SurName, MatchMode.Anywhere));
                    }
                    if (employee.Phone != "")
                    {
                        criteria.Add(Restrictions.InsensitiveLike("SurName", employee.SurName, MatchMode.Anywhere));
                    }
                    if (employee.Email != "")
                    {
                        criteria.Add(Restrictions.InsensitiveLike("SurName", employee.SurName, MatchMode.Anywhere));
                    }
                    if (employee.CountryId != null)
                    {
                        criteria.Add(Restrictions.Eq("CountryId", employee.CountryId));
                    }
                    if (employee.CityId != null)
                    {
                        criteria.Add(Restrictions.Eq("CityId", employee.CityId));
                    }

                    list = criteria.List<Employee>();
                }
            }
            return list;
        }

        public Employee AddEmployee(Employee employee)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                employee.CountryId = null;
                employee.CityId = null;
                session.Save(employee);
                transaction.Commit();
            }
            return employee;
        }

        public Employee SaveEmployee(Employee employee)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Merge(employee);
                transaction.Commit();
            }
            return employee;
        }

        public void DeleteEmployee(int employeeId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                Employee employee = session.Get<Employee>(employeeId);
                session.Delete(employee);
                transaction.Commit();
            }

        }

    }
}