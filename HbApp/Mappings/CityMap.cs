﻿using FluentNHibernate.Mapping;
using HbApp.Models;

namespace HbApp.Mappings
{
    public class CityMap : ClassMap<City>
    {
        public CityMap()
        {
            Id(x => x.CityId);
            Map(x => x.CityName);
            Map(x => x.CountryId);
        }
    }
}