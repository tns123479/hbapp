﻿using FluentNHibernate.Mapping;
using HbApp.Models;

namespace HbApp.Mappings
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Id(x => x.EmployeeId);
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.SurName);
            Map(x => x.Phone);
            Map(x => x.Email);
            Map(x => x.CountryId).Nullable(); ;          
            Map(x => x.CityId).Nullable();
        }
    }
}