﻿using FluentNHibernate.Mapping;
using HbApp.Models;

namespace HbApp.Mappings
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Id(x => x.CountryId);
            Map(x => x.CountryName);
        }
    }
}